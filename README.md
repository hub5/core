# README #

The PaymentMQPublisher class is the main class needed by the Core API to publish payments to the queue. It requires other supporting
libraries which are included in the [libraries](https://bitbucket.org/hub5/libraries).

### What is this repository for? ###

* PaymentMQPublisher API used to publish payment 'messages' to a RabbitMQ
* 1.0.0

### How do I get set up? ###

* git clone the repository


`git clone https://jeremiahmbaria@bitbucket.org/hub5/paymentmqpublisher.git`

`git clone https://bitbucket.org/hub5/libraries`

* Configuration
move the configs from the libraries folder to ../
Update the configurations for rabbitMQ in the libraries folder 
* Dependencies
composer

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* [Jeremiah Mbaria](jeremiah.mbaria@cellulant.com)
* hub@cellulant.com
