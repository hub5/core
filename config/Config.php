<?php

namespace MQPublisher;

/**
 * Configuration class for the Publusher API
 */
class Config {

    const DATE_FORMAT = "Y-m-d H:i:s";
    const LOGDIR = "/var/log/applications/api/";
    const USERNAME = 'guest';
    const PASSWORD = 'guest';
    const HOST = 'localhost';
    const PORT = 5672;
    const EXCHANGE_NAME = 'KE.HUB';
    const EXCHANGE_TYPE = 'topic';
    const COUNTRY_CODE = 'KE';
    const QUEUE_DURABILITY = 2;    // durable
    const ERROR = 401;  // error code
    const SUCCESS = 200;  // success code
    const USEAUTH = false;
    const USE_PROXY_QUEUES = true;
    const USE_EXCHANGE = false;

    //CoreAPI Configurations
    const DB_HOST = '127.0.0.1';
    const DB_USERNAME = 'root';
    const DB_PASSWORD = '!23qweASD';
    const DATABASE = 'hub_ke';
    const PAYMENT_POSTED_SUCCESSFULLY = 139;
    const AUTH_SUCCESS = 131;
    const PUBLISHER_URL = 'http://localhost:4000/api/publisher/payment/index.php';

    public static $PROXIY_QUEUES = array('SAFARICOM','AIRTEL','TELKOM');

    // source addresses that would require a dedicated queue for each message type.
    public static $SMS_QUEUE_WHITELIST = array('MULA');
    public static $PAYMENT_QUEUE_WHITELIST = array('ZUKU');
    public static $BULK_QUEUE_WHITELIST = array('STANBIC');
    public static $PREMIUM_QUEUE_WHITELIST = array();

    const REDIS_HOSTNAME = 'localhost';
    const REDIS_PORT = 6379;
    const REDIS_PASSWORD = '!23qweASD';

}
