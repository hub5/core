<?php
error_reporting(E_ALL);

require_once __DIR__ . '/../libraries/vendor/autoload.php';


use MQPublisher\Config;
use MQPublisher\Libraries\Utilities\MQLogger;
use MQPublisher\Libraries\Utilities\ExecutionTime;
use MQPublisher\Libraries\Utilities\RedisConnection;
use MQPublisher\Libraries\Utilities\Utilities;
use MQPublisher\Libraries\Utilities\CoreData;
use MQPublisher\Libraries\Utilities\DB;

class Core{

	public function __construct(){
		//instatiate the startTime needed to calculate the TAT
		$startTime = microtime(true);

		// initialise the logger for different levels of logging
		// second param allows us to specify the sub-dir to log into
        $this->logger = new MQLogger('coreAPI','core');
        
        // fetch the raw request
        $postRequest = file_get_contents("php://input");

        //convert it to stdClass object to access attributes
        $request = json_decode($postRequest);


        //TODO: route the request based on function
        
  		// access the data variable and complain incase of an error
        if (count($request->payload) > 0 and $request->function == 'BEEP.postPayment') {
            $this->logger->debug('Raw payload: ' . $request->payload, __FUNCTION__, __LINE__, $startTime, 0);

            $payload = json_decode($request->payload);

            //authenticate the request before doing anything else
            $isAuthorised = $this->authenticate($payload->credentials);

            if($isAuthorised){

                // $paymentData = $payload->packet[0];

                //validate params passed by the request
                $isValid = $this->validatePayload($payload);

                if ($isValid) {

                    $response = $this->postPayment($payload);

                }else{

                    $descp = 'Payload validation failed';
                    $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, json_encode($paymentData, true));
                }

            }else{
                $descp = 'Authentication failed';
                $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, json_encode($paymentData, true));
                $response = Utilities::response(Config::ERROR, $descp, '');
            }


        } else {
            $message = 'POST request expected';
            $this->logger->error($message, __FUNCTION__, __LINE__, $startTime, 0);
            $response = Utilities::response(Config::ERROR, $message, '');

        }

        echo $response;

	}

    /**
     * Authenticate the credentials provided
     * @param  json $credentials credentials
     * @return boolean  status
     */
    function authenticate($credentials){
        $authStatus = 0;

        $redis = new RedisConnection();
        $redis->getKey($credentials->username);

        //TODO: Need to check the hash here. This is too basic & unsafe
        if(count($redis)){
           $authStatus = 1;
        }else{
            $authStatus = 0;
        }

        return $authStatus;
    }    

    /**
     * Super function that will call all other validation functions
     * @param  json $payload request payload
     * @return boolean  status
     */
    function validatePayload($payload){
        $packet = $payload->packet[0];
        $isValid = 0;

        $isValid = $this->validateParams($packet);

        $isValid = $this->validateService($packet->serviceID);

        //fetch client details to check service settings
        $getClientDetails = CoreData::getClientDetails($payload->credentials->username);
        $isValid = $this->validateServiceSettings($packet->serviceID, $getClientDetails->clientID);

        $isDuplicate = $this->isDuplicate($packet, $getClientDetails->clientID);
        $isValid = $isDuplicate > 0 ? 0 : 1;

        return $isValid;
    }

    /**
     * Validate payload params for all indices and value formats
     * @param  json $payload request payload
     * @return boolean  status
     */
    function validateParams($payload){
        $isValid = 0;

        //this are the valid params to check the payload aganist
        $params = array(
                        'MSISDN',
                        'amount',
                        'serviceID',
                        'narration',
                        'currencyCode',
                        'invoiceNumber',
                        'accountNumber',
                        'datePaymentReceived',
                        'payerTransactionID',
                        'hubID',
                        'customerNames',
                        'paymentMode'
                    );

        $keys = array_keys((array) $payload);

        $diff = array_diff($params, $keys);

        //convert the array to csv for reporting
        $missingParams = Utilities::arraytocsv($diff);


        if(count($diff)){
            $message = 'paylod is missing: '.$missingParams;
            $this->logger->error($message, __FUNCTION__, __LINE__, $startTime, 0);
            Utilities::response(Config::ERROR, $message, '');
            $isValid = 0;
        }else{
            $isValid = 1;
        }
    
        return $isValid;
    }


    /**
     * Validate the service
     * @return boolean $serviceStatus
     */
    function validateService($serviceID){
        $isValid = 0;
        $serviceData = CoreData::getServiceData($serviceID);

        //check if service is active
        if($serviceData->sactive == 1){
            $isValid = 1;
        }else{
            $message = 'Service is not enabled, postPayment has been halted';
            $this->logger->error($message, __FUNCTION__, __LINE__, $startTime, 0);
            Utilities::response(Config::ERROR, $message, '');
            $isValid = 0;
        }

        return $isValid;
    }

    /**
     * Validate the service
     * @return boolean $serviceStatus
     */
    function validateServiceSettings($serviceID, $payerclientID){
        $isValid = 0;

        $validateServiceSettings = CoreData::getServiceSettingData($serviceID, $payerclientID);

        if(count($validateServiceSettings) > 0){
            $isValid = 1;
        }else{
            $message = 'PayerID '.$payerclientID.' is not allowed to consume this service: '.$serviceID;
            $this->logger->error($message, __FUNCTION__, __LINE__, $startTime, 0);
            Utilities::response(Config::ERROR, $message, '');
            $isValid = 0;
        }

        return $isValid;

    }


    /**
     * Check for duplicate in the database // TODO: This is best handled by a constraint rather than a query to avoid slow queries
     * @return boolean $serviceStatus
     */
    function isDuplicate($payload, $payerclientID){
        $isDuplicate = 0;

        $dbh = DB::getDBConnection();
        $query = 'select count(paymentID)found from s_payments where payerTransactionID = :payerTransactionID and payerclientID = :payerclientID';
        $stmt = $dbh->prepare($query);
        $stmt->bindParam(':payerTransactionID',$payload->payerTransactionID,PDO::PARAM_STR);
        $stmt->bindParam(':payerclientID',$payerclientID,PDO::PARAM_INT);
        $stmt->execute();
        $result = (object) $stmt->fetch(PDO::FETCH_ASSOC);

        if($result->found > 0){
            $message = 'Duplicate payment found for payerclientID:'.$payerclientID.' payerTransactionID: '.$payload->payerTransactionID;
            $this->logger->error($message, __FUNCTION__, __LINE__, $startTime, 0);
            Utilities::response(Config::ERROR, $message, '');

            $isDuplicate = 1;

        }else{
            $isDuplicate = 0;
        }

        return $isDuplicate;
    }

    /**
     * Insert payment into batchRequests,s_payments and s_requestLogs tables
     * @return json $response status of posting
     */
    function postPayment($payload){
        $packet = $payload->packet[0];

        //get client details for the api user posting the payment
        $clientData = CoreData::getClientDetails($payload->credentials->username);

        $dbh = DB::getDBConnection();

        $serviceData = CoreData::getServiceData($packet->serviceID);

        // insert into s_requestBatches
        try{
            $query = "insert into s_requestBatches(totalRequests, batchStatus, statusHistory) values(:totalRequests, :batchStatus, :statusHistory)";
            $stmt = $dbh->prepare($query);

            $totalRequests = 1;
            $batchStatus = 1;
            $statusHistory = 0;
            $stmt->bindParam(':totalRequests',$totalRequests,PDO::PARAM_INT);
            $stmt->bindParam(':batchStatus',$batchStatus,PDO::PARAM_INT);
            $stmt->bindParam(':statusHistory',$statusHistory,PDO::PARAM_INT);

            if($stmt->execute()){
                $message = 's_requestBatches entry added successfully';
                $this->logger->info($message, __FUNCTION__, __LINE__, $startTime, 0);
                $requestBatchID = $dbh->lastInsertId();
            }else{
                $message = 's_requestBatches entry failed';
                $this->logger->error($message, __FUNCTION__, __LINE__, $startTime, 0);
                $requestBatchID = 0;
            }
        }catch(PDOException $ex){
            $message = 'PDOException '.$ex->getMessage();
            $this->logger->error($message, __FUNCTION__, __LINE__, $startTime, 0);
        }catch(Exception $ex){
            $message = 'Exception '.$ex->getMessage();
            $this->logger->error($message, __FUNCTION__, __LINE__, $startTime, 0);
        }

        // insert into s_requestLogs
        if($requestBatchID){
            try{
                $query = "insert into s_requestLogs(requestBatchID, hubID, 
                    receiverClientID, serviceID, invoiceNumber, accountNumber, 
                    MSISDN, narration, paymentMode, overallStatus, requestOriginID, 
                    languageID, dateCreated, overallStatusHistory, 
                    insertedBy, updatedBy)
                    values(:requestBatchID, :hubID, 
                    :receiverClientID, :serviceID, :invoiceNumber, :accountNumber, 
                    :MSISDN, :narration, :paymentMode, :overallStatus, :requestOriginID, 
                    :languageID, :dateCreated, :overallStatusHistory, 
                    :insertedBy, :updatedBy
                )";
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
                $stmt = $dbh->prepare($query);

                $invoiceNumber = NULL;
                $overallStatus = Config::PAYMENT_POSTED_SUCCESSFULLY;
                $requestOriginID = 6;
                $languageID = NULL;
                $dateCreated = Utilities::dateTime();
                $overallStatusHistory = Config::PAYMENT_POSTED_SUCCESSFULLY.'|'.$clientData->userID.'|'.Utilities::dateTime();

                $stmt->bindParam(':requestBatchID',$requestBatchID,PDO::PARAM_INT);
                $stmt->bindParam(':hubID',$packet->hubID,PDO::PARAM_INT);
                $stmt->bindParam(':receiverClientID',$serviceData->ownerClientID,PDO::PARAM_INT);
                $stmt->bindParam(':serviceID',$packet->serviceID,PDO::PARAM_INT);
                $stmt->bindParam(':invoiceNumber',$invoiceNumber,PDO::PARAM_INT);
                $stmt->bindParam(':accountNumber',$packet->accountNumber,PDO::PARAM_INT);
                $stmt->bindParam(':MSISDN',$packet->MSISDN,PDO::PARAM_INT);
                $stmt->bindParam(':narration',$packet->narration,PDO::PARAM_INT);
                $stmt->bindParam(':paymentMode',$packet->paymentMode,PDO::PARAM_INT);
                $stmt->bindParam(':overallStatus',$overallStatus,PDO::PARAM_INT);
                $stmt->bindParam(':requestOriginID',$requestOriginID,PDO::PARAM_INT);
                $stmt->bindParam(':languageID',$languageID,PDO::PARAM_INT);
                $stmt->bindParam(':dateCreated',$dateCreated,PDO::PARAM_INT);
                $stmt->bindParam(':overallStatusHistory',$overallStatusHistory,PDO::PARAM_STR);
                $stmt->bindParam(':insertedBy',$clientData->userID,PDO::PARAM_INT);
                $stmt->bindParam(':updatedBy',$clientData->userID,PDO::PARAM_INT);

                if($stmt->execute()){
                    $message = 's_requestLogs entry added successfully';
                    $this->logger->info($message, __FUNCTION__, __LINE__, $startTime, 0);
                    $requestLogID = $dbh->lastInsertId();
                }else{
                    $message = 's_requestLogs entry failed ';
                    $this->logger->error($message, __FUNCTION__, __LINE__, $startTime, 0);
                    $requestLogID = 0;
                }
            }catch(PDOException $ex){
                $message = 'PDOException '.$ex->getMessage();
                $this->logger->error($message, __FUNCTION__, __LINE__, $startTime, 0);
            }catch(Exception $ex){
                $message = 'Exception '.$ex->getMessage();
                $this->logger->error($message, __FUNCTION__, __LINE__, $startTime, 0);
            }

        }

        // insert into s_payments
        if($requestLogID){
            try{
                $query = "INSERT INTO s_payments (requestLogID, currencyID, 
                    payerClientID, serviceID, customerName, amountPaid, MSISDN, 
                    payerNarration, payerTransactionID, paymentDate, extraData, 
                    accessPointID, parentPaymentID, originatorClientID, 
                    merchantTierID, conversionRateID, actualConversionRate, 
                    convertedAmount, convertedCurrencyID, isMNP, dateCreated, 
                    insertedBy, updatedBy) 
                    VALUES (:requestLogID, :currencyID, 
                    :payerClientID, :serviceID, :customerName, :amountPaid, :MSISDN, 
                    :payerNarration, :payerTransactionID, :paymentDate, :extraData, 
                    :accessPointID, :parentPaymentID, :originatorClientID, 
                    :merchantTierID, :conversionRateID, :actualConversionRate, 
                    :convertedAmount, :convertedCurrencyID, :isMNP, :dateCreated, 
                    :insertedBy, :updatedBy
                )";

                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
                $stmt = $dbh->prepare($query);

                $extraData = isset($packet->extraData) ? $packet->extraData : '';
                $accessPoint = isset($packet->accessPoint) ? $packet->accessPoint : NULL;
                $parentPaymentID = NULL;//isset($packet->originalPaymentID) and !empty($packet->originalPaymentID) ? $packet->originalPaymentID : NULL;
                $originatorClientID = NULL;//isset($packet->originalPayerClientID) and !empty($packet->originalPayerClientID) ? $packet->originalPayerClientID : NULL;
                $merchantTierID = NULL;//isset($packet->merchantTierCode) and !empty($packet->merchantTierCode) ? $packet->merchantTierCode : '';

                //Currency Conversion values
                $conversionRateID = NULL;
                $actualConversionRate = NULL;
                $convertedAmount = NULL;
                $convertedCurrencyID = NULL;

                $isMNP = FALSE;
                $dateCreated = Utilities::dateTime();


                $stmt->bindParam(':requestLogID',$requestLogID,PDO::PARAM_INT);
                $stmt->bindParam(':currencyID',$serviceData->currencyID,PDO::PARAM_INT);
                $stmt->bindParam(':payerClientID',$clientData->clientID,PDO::PARAM_INT);
                $stmt->bindParam(':serviceID',$packet->serviceID,PDO::PARAM_INT);
                $stmt->bindParam(':customerName',$packet->customerNames,PDO::PARAM_STR);
                $stmt->bindParam(':amountPaid',$packet->amount,PDO::PARAM_INT);
                $stmt->bindParam(':MSISDN',$packet->MSISDN,PDO::PARAM_INT);
                $stmt->bindParam(':payerNarration',$packet->narration,PDO::PARAM_STR);
                $stmt->bindParam(':payerTransactionID',$packet->payerTransactionID,PDO::PARAM_INT);
                $stmt->bindParam(':paymentDate',$packet->datePaymentReceived,PDO::PARAM_STR);
                $stmt->bindParam(':extraData',$extraData,PDO::PARAM_STR);
                $stmt->bindParam(':accessPointID',$accessPoint,PDO::PARAM_INT);
                $stmt->bindParam(':parentPaymentID',$parentPaymentID,PDO::PARAM_INT);
                $stmt->bindParam(':originatorClientID',$originatorClientID,PDO::PARAM_INT);
                $stmt->bindParam(':merchantTierID',$merchantTierID,PDO::PARAM_INT);
                $stmt->bindParam(':conversionRateID',$conversionRateID,PDO::PARAM_INT);
                $stmt->bindParam(':actualConversionRate',$actualConversionRate,PDO::PARAM_INT);
                $stmt->bindParam(':convertedAmount',$convertedAmount,PDO::PARAM_INT);
                $stmt->bindParam(':convertedCurrencyID',$convertedCurrencyID,PDO::PARAM_INT);
                $stmt->bindParam(':isMNP',$isMNP,PDO::PARAM_INT);
                $stmt->bindParam(':dateCreated',$dateCreated,PDO::PARAM_STR);
                $stmt->bindParam(':insertedBy',$clientData->userID,PDO::PARAM_INT);
                $stmt->bindParam(':updatedBy',$clientData->userID,PDO::PARAM_INT);

                if($stmt->execute()){
                    $message = 's_payments entry added successfully';
                    $this->logger->info($message, __FUNCTION__, __LINE__, $startTime, 0);
                    $paymentID = $dbh->lastInsertId();
                }else{
                    $message = 's_payments entry failed ';
                    $this->logger->error($message, __FUNCTION__, __LINE__, $startTime, 0);
                    $paymentID = 0;
                }
            }catch(PDOException $ex){
                $message = 'PDOException '.$ex->getMessage();
                $this->logger->error($message, __FUNCTION__, __LINE__, $startTime, 0);
            }catch(Exception $ex){
                $message = 'Exception '.$ex->getMessage();
                $this->logger->error($message, __FUNCTION__, __LINE__, $startTime, 0);
            }
        }

        //convert array object to array
        $payloadArray = (array) $packet;
        $payloadArray['requestLogID'] = $requestLogID;
        $payloadArray['clientCode'] = $clientData->clientCode;
        $payloadArray['serviceCode'] = $serviceData->serviceCode;

        //publish to queue
        $this->publishToQueue((object) $payloadArray);

        //check that we have all the relevant ids before we can confidently say we did a postPayment
        if($requestBatchID > 0 and $requestLogID > 0 and $paymentID > 0){
            $authStatus = array(
                'authStatusCode' => (int) Config::AUTH_SUCCESS,
                'authStatusDescription' => 'Authentication was a success'
            );

            $paymentStatus = array(
                'results' => array(
                    'statusCode' => (int) Config::PAYMENT_POSTED_SUCCESSFULLY,
                    'statusDescription' => 'Payment pending acknowledgement',
                    'payerTransactionID' => $packet->payerTransactionID,
                    'beepTransactionID' => (int) $requestLogID,
                    'invoiceNumber' => -1
                )
            );

            echo json_encode(array($authStatus,$paymentStatus));
        }
    }

    /**
     * Publish the payload to the MQ server
     * @return json $response publist status
     */
    function publishToQueue($payload){

        $publishData = array(
            'AUTH' => array(
                'USERNAME' => 'KE.AUTH.smsapi',
                'PASSWORD' => '5m5@p!'
            ),
            'PAYLOAD' => array(
                'PAYMENTID' => $payload->requestLogID,
                'MSISDN' => $payload->MSISDN,
                'CLIENTCODE' => $payload->clientCode,
                'SERVICECODE' => $payload->serviceCode,
                'DATETIME' => Utilities::dateTime(),
                'REPUSH' => 0,
                'NUMBEROFSENDS' => 0,
                'COUNTRYCODE' => Config::COUNTRY_CODE,
                'MESSAGETYPE' => "PAYMENT"
            ),
            'TYPE' => 'OUT'
        );

        $publishData = json_encode($publishData);
        $result = Utilities::post(Config::PUBLISHER_URL,array('data' => $publishData));

        return $result;
    }

}
