<?php
error_reporting(E_ALL);

require_once __DIR__ . '/../libraries/vendor/autoload.php';


use MQPublisher\Config;
use MQPublisher\Libraries\Utilities\MQLogger;
use MQPublisher\Libraries\Utilities\ExecutionTime;
use MQPublisher\Libraries\Utilities\MQConnection;
use MQPublisher\Libraries\Utilities\RedisConnection;

use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

use PhpAmqpLib\Exception;
use PhpAmqpLib\AMQPTimeoutException;
use PhpAmqpLib\AMQPExchangeException;
use PhpAmqpLib\Exception\AMQPProtocolChannelException;

class PaymentMQPublisher{

	public function __construct(){
		//instatiate the startTime needed to calculate the TAT
		$startTime = microtime(true);

		// initialise the logger for different levels of logging
		// second param allows us to specify the sub-dir to log into
        $this->logger = new MQLogger('paymentPublisherAPI','payment');
        
        // convert request to JSON for easier handling
  		$request = (object) $_POST;

  		// access the data variable and complain incase of an error
        if (isset($request->data)) {
            $this->logger->debug('submitted raw payload: ' . $request->data, __FUNCTION__, __LINE__, $startTime, 0);

            $request = $request->data;

            //validate params passed by the request
            $valid = $this->validatePayload($request);
            if ($valid) {
                
                $response = $this->processRequest($request);
            }else{

                $descp = 'Payload validation failed';
                $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, json_encode($request, true));
            }

        } else {
            $message = 'POST request expected';
            $this->logger->error($message, __FUNCTION__, __LINE__, $startTime, 0);
            $response = $this->response(Config::ERROR, $message, '');

        }

        echo $response;

	}

	/**Process the request after validation i.e auth,publish
     * @param  ArrayObject $request raw payload request
     * @return JSON $response JSON response after processing
     */
    function processRequest($request){
        //establish MQ connection
        $connection = new MQConnection();

        //establish redis connection
        $redis = new RedisConnection();

        // authenticate the supplied credentials before proceeding to publish the message to queue
        if($redis->getKey($request->AUTH->USERNAME) == $request->AUTH->PASSWORD and Config::USEAUTH == true){

            $descp = 'Authentication successful, proceeding to publish message, username:' . $request->AUTH->USERNAME . ' password:*****';
            $this->logger->info($descp, __FUNCTION__, __LINE__, $startTime, print_r($request, true));

            //publish to queue
            $response = $this->MQPublish($request, $connection);

        }elseif(Config::USEAUTH == false){
            $response = $this->MQPublish($request, $connection);

        }elseif(!isset($redis)){

            $descp = 'unable to authenticate in cache, Authentication failure for username:' . $request->AUTH->USERNAME . ' password:*****';
            $response = $this->response(Config::ERROR, $descp, '');
            $this->logger->info($descp, __FUNCTION__, __LINE__, $startTime, print_r($request, true));

        }else{

            $descp = 'Unable to authenticate, check redis cache if it\'s up if the key exists';
            $response = $this->response(Config::ERROR, $descp, '');
            $this->logger->info($descp, __FUNCTION__, __LINE__, $startTime, print_r($request, true));
        }

        return $response;
    }

	/**
     * Publish message to the queue
     * @param Object $payload Payload in JSON object
     */
    public function MQPublish($payload, $connection) {

        //instatiate the startTime needed to calculate the TAT
        $startTime = microtime(true);

        $channel = $connection->createMQConnection();

        if(count($channel) == 0 and $channel == 0){
            $descp = 'We were unable to create MQ connection';
            $resp = $this->response(Config::ERROR, $descp, '');
            return $resp;
        }

        //decode the payload to access the sourceaddr
        $data = json_decode($payload);

        //accept the payload only when we have a connection and the payload is valid
        if (isset($channel)) {
            try {

                $queueName = $this->resolveQueueName($data->PAYLOAD,$data->TYPE);

                //declare queue here if it doesn't exist, if it exists it will not be recreated
                 $channel->queue_declare(
                  $queueName, // queue name
                  false, // passive
                  Config::QUEUE_DURABILITY, // durability
                  false,// exclusive
                  false, // autodelete
                  false, // no_wait
                  null, // arguments
                  null // tickets
                  ); 

                //create a message object that is persistent
                $message = new AMQPMessage(json_encode($data->PAYLOAD), array('delivery_mode' => Config::QUEUE_DURABILITY));
                $descp = 'data payload is: ' . json_encode($data->PAYLOAD);
                $this->logger->debug($descp, __FUNCTION__, __LINE__, $startTime, -1);

                if(Config::USE_EXCHANGE){
                    
                    // declare an exchange to use
                    $channel->exchange_declare(
                      Config::EXCHANGE_NAME, // exchange name
                      Config::EXCHANGE_TYPE, // exchange type
                      false, //passive
                      true, // durability
                      false, // auto delete
                      false // internal
                    );

                    // create a binding between an exchange and a queue. This can also be done manually via the mgt console
                    $channel->queue_bind(
                        $queueName, //queue name
                        Config::EXCHANGE_NAME, // exchange name
                        $queueName //binding key
                    );
                }

                // publish the message to the exchange if configs allow
                if(Config::USE_EXCHANGE){
                    $channel->basic_publish($message, Config::EXCHANGE_NAME, $queueName);
                }else{
                    $channel->basic_publish($message, '', $queueName);
                }

                // return a 200 response on successful publish
                $descp = 'Publish successful to ' . $queueName;
                $this->logger->info($descp, __FUNCTION__, __LINE__, $startTime, $data->PAYLOAD->PAYMENTID);
                $response = $this->response(Config::SUCCESS, $descp, '');

                // close the channel after publish
                $channel->close();
            } catch (Exception $e) {

                $descp = 'Publish Exception 1 - ' . $e->getMessage();
                $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, $data->PAYLOAD->PAYMENTID);
                $this->response(Config::ERROR, $descp, '');
            } catch (AMQPTimeoutException $e) {

                $descp = 'Publish Exception 2 - ' . $e->getMessage();
                $response = $this->response(Config::ERROR, $descp, '');

                $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, $data->PAYLOAD->PAYMENTID);
            } catch (AMQPExchangeException $e) {

                $descp = 'Publish Exception 3 - ' . $e->getMessage();
                $response = $this->response(Config::ERROR, $descp, '');

                $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, $data->PAYLOAD->PAYMENTID);
            }
        } else {

            // report missing channel connection 
            $descp = 'Something went wrong. Raise it with hub@cellulant for resolution.';
            $this->logger->info($descp, __FUNCTION__, __LINE__, $startTime, $payload);
            $response = $this->response(Config::ERROR, $descp, '');
        }

        return $response;
    }


    /**Resolves the proper queue name to use based on configurations
     * @param  ArrayObject $payload
     * @param  String payload
     * @return String payload
     */
    function resolveQueueName($payload,$queueType){

        //get sourceaddr from payload
        $SERVICECODE = strtoupper($payload->SERVICECODE); 

        //get proxyname from payload
        $PROXYNAME = strtoupper(str_ireplace('Ke_','',$payload->PROXYNAME));

        // formulae for creating queues is below. Final queue should be like : TYPE.COUNTRYCODE.MESSAGETYPE.[PROXYNAME].[.SERVICECODE  --optional]
        if (in_array($SERVICECODE, Config::$PAYMENT_QUEUE_WHITELIST) == true) {
            
            $queueName = $queueType . '.' . $payload->COUNTRYCODE . '.' . $payload->MESSAGETYPE .'.'. $SERVICECODE;
        }else {
            
            $queueName = $queueType . '.' . $payload->COUNTRYCODE . '.' . $payload->MESSAGETYPE;
        }

        return $queueName;
    }


	/**
	 * Formats response as JSON
	 * @param  responseCode $code  Int
	 * @param  Description $descp Description of response
	 * @param  Data $data  Array of data in response
	 * @return String Object        Json object
	 */
	public function response($code,$descp,$data){
		$response = array();
        $response['statusDescription'] = $descp;
        if(isset($data))
        	$response['data'] = $data;

        $response['code'] = $code;

        echo json_encode($response);
	}


	/**
     * Validates payment payload for mandatory fields
     * @param  Object $payload Payload submitted by client
     * @return Int true/false
     */
    public function validatePayload($payload) {
        //instatiate the startTime needed to calculate the TAT

        $startTime = microtime(true);

        //decompile to determine the message type i.e USSD|SMS|BULK|SMS|PREMIUM
        $payload = json_decode($payload);

        // check if the request is an OUT/IN
        if (strtoupper($payload->TYPE) == 'OUT') {

            if (!isset($payload->AUTH) or empty($payload->AUTH)) {

                $descp = 'Your payload is missing the authentication property.';
                $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);
                $resp = $this->response(Config::ERROR, $descp, '');
                return $resp;
            } elseif (!isset($payload->TYPE) or $payload->TYPE == '') {
                // check if message type is defined i.e OUT[going] or IN[coming]

                $descp = 'Message type not defined. Format should be OUT/IN';
                $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);
                $resp = $this->response(Config::ERROR, $descp, '');
                return $resp;
            } elseif (count($payload->PAYLOAD) > 0) {


                // check if payload is in JSON format
                if (!in_array($payload->PAYLOAD->MESSAGETYPE, array('PAYMENT'))) {

                    $descp = 'Improper payload format for PAYMENT';
                    $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);
                    $resp = $this->response(Config::ERROR, $descp, '');
                    return $resp;
                } elseif (empty($payload->PAYLOAD->MESSAGETYPE) or $payload->PAYLOAD->MESSAGETYPE == '') {
                    // check for empty PAYMENT value

                    $descp = 'PAYMENT cannot be empty|NULL|string';
                    $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);
                    $resp = $this->response(Config::ERROR, $descp, '');
                    return $resp;
                }

                // TODO: Should REPUSH be checked here since the Retry Daemon will invoke this API?
                if (!isset($payload->PAYLOAD->REPUSH)) {
                    $descp = 'REPUSH attribute is not set';
                    $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);
                    $resp = $this->response(Config::ERROR, $descp, '');

                    return $resp;
                }

                if (!isset($payload->PAYLOAD->MSISDN)) {
                    // check for missing MSISDN attribute

                    $descp = 'MSISDN attribute is not set';
                    $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);
                    $resp = $this->response(Config::ERROR, $descp, '');
                    return $resp;
                } elseif (empty($payload->PAYLOAD->MSISDN) or $payload->PAYLOAD->MSISDN == '') {
                    // check for empty MSISDN value

                    $descp = 'MSISDN cannot be empty|NULL';
                    $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);
                    $resp = $this->response(Config::ERROR, $descp, '');
                    return $resp;
                }

                if (!isset($payload->PAYLOAD->COUNTRYCODE)) {
                    // check for missing COUNTRYCODE attribute

                    $descp = 'COUNTRYCODE attribute is not set';
                    $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);
                    $resp = $this->response(Config::ERROR, $descp, '');
                    return $resp;
                } elseif (empty($payload->PAYLOAD->COUNTRYCODE) or $payload->PAYLOAD->COUNTRYCODE == '') {
                    // check for empty COUNTRYCODE value

                    $descp = 'COUNTRYCODE cannot be empty|NULL';
                    $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);
                    $resp = $this->response(Config::ERROR, $descp, '');
                    return $resp;
                }

                if (!isset($payload->PAYLOAD->NUMBEROFSENDS)) {
                    $descp = 'NUMBEROFSENDS attribute is not set';
                    $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);
                    $resp = $this->response(Config::ERROR, $descp, '');
                    return $resp;
                } elseif (is_nan($payload->PAYLOAD->NUMBEROFSENDS)) {
                    $descp = 'NUMBEROFSENDS cannot be empty|NULL|string';
                    $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);
                    $this->response(Config::ERROR, $descp, '');
                    return $resp;
                }

                if (!isset($payload->PAYLOAD->SERVICECODE)) {
                    $descp = 'SERVICECODE attribute is not set';
                    $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);
                    $resp = $this->response(Config::ERROR, $descp, '');
                    return $resp;
                } elseif (is_nan($payload->PAYLOAD->SERVICECODE)) {
                    $descp = 'SERVICECODE cannot be empty|NULL|string';
                    $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);
                    $resp = $this->response(Config::ERROR, $descp, '');
                    return $resp;
                }

                if (!isset($payload->PAYLOAD->PAYMENTID)) {
                    $descp = 'PAYMENTID attribute is not set';
                    $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);
                    $resp = $this->response(Config::ERROR, $descp, '');
                    return $resp;
                } elseif (is_nan($payload->PAYLOAD->PAYMENTID)) {
                    $descp = 'PAYMENTID cannot be empty|NULL';
                    $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);
                    $resp = $this->response(Config::ERROR, $descp, '');
                    return $resp;
                }

                if (!isset($payload->PAYLOAD->DATETIME)) {
                    $descp = 'DATETIME attribute is not set';
                    $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);
                    $resp = $this->response(Config::ERROR, $descp, '');
                    return $resp;
                } elseif ($payload->PAYLOAD->DATETIME == '' or empty($payload->PAYLOAD->DATETIME)) {
                    // check for empty DATETIME

                    $descp = 'DATETIME should not be NULL';
                    $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);
                    $resp = $this->response(Config::ERROR, $descp, '');
                    return $resp;
                }

                $this->logger->info('Payload validated successfully, continuing...', __FUNCTION__, __LINE__, $startTime, 0);

                return 1;
            }
        } else {
            //for IN queue type, pass validation
            return 1;
        }
    }
}
