<?php
namespace MQPublisher\Libraries\Utilities;

use MQPublisher\Config;
use MQPublisher\Libraries\Utilities\MQLogger;
use MQPublisher\Libraries\Utilities\ExecutionTime;

use Predis\Client as PredisClient;
use Predis\Connection;
use Predis\Connection\ConnectionException;
use Predis\Connection\CommunicationException;
use Predis\Connection\AbstractConnection;

use MQPublisher\Libraries\Utilities\Utilities;

class RedisConnection{

    /**
     * Initialise the logging library
     */
    public function __construct(){
        //initialise the logger for different levels of logging
		// second param allows us to specify the sub-dir to log into
        $this->logger = new MQLogger('publisherAPI','');
        $redis = $this->createRedisConnection();

        return $redis;
    }

    /**
     * Create a connection to the redis server
     *
     * @return Object Redis connection object
     */
    public function createRedisConnection(){
        //instatiate the startTime needed to calculate the TAT
        $startTime = microtime(true);

        $redisConnectionState = 0;
        
        try {
            $redis = new PredisClient([
                "scheme" => "tcp",
                "host" => Config::REDIS_HOSTNAME,
                "port" => Config::REDIS_PORT,
                "password" => Config::REDIS_PASSWORD,
                "persistent" => "1"]);
            $redis->connect();

            $redisConnectionState = $redis;
        }catch(ConnectionException $e){
            $this->logger->emergency($e->getMessage(), __FUNCTION__, __LINE__, $startTime, 0);
            $descp = 'Redis Connection Exception - '.$e->getMessage();            
            // Utilities::response(Config::ERROR,$descp,'');

            $redisConnectionState = 0;
        }catch(CommunicationException $e){
            $this->logger->emergency($e->getMessage(), __FUNCTION__, __LINE__, $startTime, 0);
            $descp = 'Redis Communication Exception - '.$e->getMessage();
            // Utilities::response(Config::ERROR,$descp,'');

            $redisConnectionState = 0;
        }catch (Exception $e) {
            $this->logger->emergency($e->getMessage(), __FUNCTION__, __LINE__, $startTime, 0);
            $descp = 'Redis Generic Exception - '.$e->getMessage();
            // Utilities::response(Config::ERROR,$descp,'');

            $redisConnectionState = 0;
        }

        return $redisConnectionState;
    }

    /**
     * Access key value from the redis cache
     *
     * @param String $key
     * @return String Value matching the key in redis cache
     */
    public function getKey($key){
		$redis = $this->createRedisConnection();
                if($redis == 0){
                    return 0;
                }else{
                    $response =  $redis->get($key);
                }

		return $response;
	}
}