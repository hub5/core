<?php
namespace MQPublisher\Libraries\Utilities;

require_once __DIR__ . '/ExecutionTime.php';
require_once __DIR__ . '/../../config/Config.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Monolog\Formatter\LineFormatter;
use MQPublisher\Config;

use MQPublisher\Libraries\Utilities\ExecutionTime;

/**
 * Single class the ecapsulates the Monolog logging library to use in the MQPublisher API
 */
class MQLogger{

	protected $channel = NULL;
	protected $output = "%datetime% | %channel%.%level_name% | %message% %context% %extra%\n";


	public function __construct($channel, $type){
		$this->channel   = $channel;
		$this->type      = $type;
		$this->execTimer = new ExecutionTime();

	}

	/**
	 * Log information about the transaction/messages being processed
	 *
	 * @param String $string String describing what is logged
	 * @param String $function function being executed
	 * @param Int $line line number of function executed
	 * @param Timestamp $startTime  Timestamp of when execution started
	 * @param Int $messageId id of the message being processed
	 * @return void
	 */
	public function info($string, $function, $line, $startTime, $messageId){
		$logger = new Logger($this->channel);
		$stream = new StreamHandler(Config::LOGDIR.$this->type.'/info.log', Logger::INFO);

		$dateFormat = $this->execTimer->time();

		// finally, create a formatter
		$formatter = new LineFormatter($this->output, $dateFormat);
		$stream->setFormatter($formatter);

        $logger = $logger->pushHandler($stream);
        $logger->info($this->preLog($string, $function, $line, $startTime, $messageId));
	}

	/**
	 * Log errors that need attention
	 *
	 * @param String $string String describing what is logged
	 * @param String $function function being executed
	 * @param Int $line line number of function executed
	 * @param Timestamp $startTime  Timestamp of when execution started
	 * @param Int $messageId id of the message being processed
	 * @return void
	 */
	public function error($string, $function, $line, $startTime, $messageId){
		$logger = new Logger($this->channel);
		$stream = new StreamHandler(Config::LOGDIR.$this->type.'/error.log', Logger::ERROR);

		$dateFormat = $this->execTimer->time();

		// finally, create a formatter
		$formatter = new LineFormatter($this->output, $dateFormat);
		$stream->setFormatter($formatter);

        $logger = $logger->pushHandler($stream);
        $logger->error($this->preLog($string, $function, $line, $startTime, $messageId));
	}

	/**
	 * Log verbose information about the request being processed to assist in debugging
	 *
	 * @param String $string String describing what is logged
	 * @param String $function function being executed
	 * @param Int $line line number of function executed
	 * @param Timestamp $startTime  Timestamp of when execution started
	 * @param Int $messageId id of the message being processed
	 * @return void
	 */
	public function debug($string, $function, $line, $startTime, $messageId){
		$logger = new Logger($this->channel);
		$stream = new StreamHandler(Config::LOGDIR.$this->type.'/debug.log', Logger::DEBUG);

		$dateFormat = $this->execTimer->time();

		// finally, create a formatter
		$formatter = new LineFormatter($this->output, $dateFormat);
		$stream->setFormatter($formatter);

        $logger = $logger->pushHandler($stream);
        $logger->debug($this->preLog($string, $function, $line, $startTime, $messageId));
	}

	/**
	 * Log critical errors that need immediate attention
	 *
	 * @param String $string String describing what is logged
	 * @param String $function function being executed
	 * @param Int $line line number of function executed
	 * @param Timestamp $startTime  Timestamp of when execution started
	 * @param Int $messageId id of the message being processed
	 * @return void
	 */
	public function emergency($string, $function, $line, $startTime, $messageId){
		$logger = new Logger($this->channel);
		$stream = new StreamHandler(Config::LOGDIR.$this->type.'/fatal.log', Logger::EMERGENCY);

		$dateFormat = $this->execTimer->time();

		// finally, create a formatter
		$formatter = new LineFormatter($this->output, $dateFormat);
		$stream->setFormatter($formatter);


        $logger = $logger->pushHandler($stream);
        $logger->emergency($this->preLog($string, $function, $line, $startTime, $messageId));
	}

	/**
	 * Undocumented function
	 *
	 * @param String $message Message to be logged
	 * @param String $function function being executed
	 * @param String $line	line number of function executed
	 * @param Timestamp $startTime Timestamp of when execution started
	 * @param Int $messageId id of the message being processed
	 * @return String
	 */
	public function preLog($message, $function, $line, $startTime, $messageId){
		// TIMESTAMP | COUNTRYCODE | MESSAGETYPE | MESSAGEID | FUNCTIONNAME | LINE # | TAT | STATUS | MESSAGE
		$tat = $this->execTimer->diff($startTime);
		if(count($message) > 0){
			$message = json_encode($message);
		}
		return 'COUNTRYCODE:' . Config::COUNTRY_CODE
                        . ' | MESSAGEID: '.$messageId.' | FUNCTION: '.$function 
                        . ' | LINE: '.$line.' | TAT: '.$tat.' | MESSAGE: '.$message;
	}
}