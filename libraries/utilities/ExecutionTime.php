<?php
namespace MQPublisher\Libraries\Utilities;

/**
 * Class to exposes time functions to be used within the publisher API
 */
class ExecutionTime
{
     public $endTime;
    
     /**
      * Captures transaction end time
      *
      * @return Int Timestamp
      */
     public function end(){
        $this->endTime =  microtime(true);
        
     }

     /**
      * Calculate the different between start and end time
      *
      * @param Int $startTime Timestamp
      * @return void
      */
     public function diff($startTime){
        $this->end();// execute end before we get the diff
        return round($this->endTime - $startTime,2);
     }

     /**
      * Capture the current timestamp with milisenconds
      *
      * @return String Timestamp
      */
     public function time(){
        $date = \DateTime::createFromFormat('U.u', microtime(TRUE));
        $date->setTimeZone(new \DateTimeZone('Africa/Nairobi'));
        return $date->format('Y-m-d H:i:s.u');
     }
 }