<?php

namespace MQPublisher\Libraries\Utilities;

//include essential libs needed for recording tat and configs
require_once __DIR__ . '/ExecutionTime.php';
require_once __DIR__ . '/Utilities.php';
require_once __DIR__ . '/../../config/Config.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\Exception;
use PhpAmqpLib\Exception\AMQPException;
use PhpAmqpLib\Exception\AMQPChannelException;
use PhpAmqpLib\Exception\AMQPConnectionException;
use PhpAmqpLib\Exception\AMQPProtocolConnectionException;
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use MQPublisher\Config;
use MQPublisher\Libraries\Utilities\MQLogger;
use MQPublisher\Libraries\Utilities\ExecutionTime;
use MQPublisher\Libraries\Utilities\Utilities;

class MQConnection {

    public function __construct() {
        $this->execTimer = new ExecutionTime();

        //leaving the second parameter empty allow for us to log mqconnection logs in the parent dir
        $this->logger = new MQLogger('publisherAPI', '');
    }

    /**
     * Creates connection to the Rabbit MQ server using credentials supplied in the config file
     * @return String Object containing the status code, description and optionally data
     */
    public function createMQConnection() {
        //instatiate the startTime needed to calculate the TAT
        $startTime = microtime(true);
        $connectionState = 0;

        try {
            //create a connection to MQ Server
            $connection = new AMQPConnection(Config::HOST, Config::PORT, Config::USERNAME, Config::PASSWORD);

            if (!isset($connection)):
                echo $descp = "Could not connect to MQ Server. Check if it's up and running";
                $this->logger->error($descp, __FUNCTION__, __LINE__, $startTime, 0);

                $connectionState = 0;
            elseif (!$connection->isConnected()):
                echo $descp = "Connection to MQ Server doesn't exist";
                $this->logger->info($descp, __FUNCTION__, __LINE__, $startTime, 0);

                $connectionState = 0;
            elseif (isset($connection)):
                $descp = "Connection to MQ Server successful";
                $this->logger->info($descp, __FUNCTION__, __LINE__, $startTime, 0);

                // Create a connection to channel
                $channel = new AMQPChannel($connection);

                $connectionState = $channel;
            else:
                $descp = 'Unable to connect';
                Utilities::response(Config::ERROR, $descp, '');
                $this->logger->emergency($descp, __FUNCTION__, __LINE__, $startTime, 0);

                $connectionState = 0;
            endif;
        } catch (AMQPConnectionException $e) {
            $descp = 'AMQP Connection Exception - ' . $e->getMessage();
            Utilities::response(Config::ERROR, $descp, '');
            $this->logger->emergency($descp, __FUNCTION__, __LINE__, $startTime, 0);

            $connectionState = 0;
        } catch (AMQPChannelException $e) {
            $descp = 'AMQP Channel Exception - ' . $e->getMessage();
            Utilities::response(Config::ERROR, $descp, '');
            $this->logger->emergency($descp, __FUNCTION__, __LINE__, $startTime, 0);

            $connectionState = 0;
        } catch (AMQPProtocolConnectionException $e) {
            $descp = 'AMQP Protocol Connection Exception - ' . $e->getMessage();
            $this->logger->emergency($descp, __FUNCTION__, __LINE__, $startTime, 0);

            $connectionState = 0;
        } catch (AMQPException $e) {
            $descp = 'AMQP Exception - ' . $e->getMessage();
            Utilities::response(Config::ERROR, $descp, '');
            $this->logger->emergency($descp, __FUNCTION__, __LINE__, $startTime, 0);

            $connectionState = 0;
        } catch (\ErrorException $e) {
            $descp = 'AMQP Exception: - ' . $e->getMessage() . '. Check if MQ server is running';
            $this->logger->emergency($descp, __FUNCTION__, __LINE__, $startTime, 0);
            Utilities::response(Config::ERROR, $descp, '');

            $connectionState = 0;
        }catch (AMQPTimeoutException $e) {
            $descp = 'AMQP TimeoutException: - ' . $e->getMessage();
            $this->logger->emergency($descp, __FUNCTION__, __LINE__, $startTime, 0);
            Utilities::response(Config::ERROR, $descp, '');

            $connectionState = 0;
        } catch (Exception $e) {
            $descp = 'AMQP Exception: - ' . $ex->getMessage();
            $this->logger->emergency($descp, __FUNCTION__, __LINE__, $startTime, 0);
            Utilities::response(Config::ERROR, $descp, '');

            $connectionState = 0;
        }

        return $connectionState;
    }

    /**
     * Declare a channel to use to consumer or publish
     * @param  Object $connection MQ connection object
     * @return Object Channel object
     */
    public function declareChannel($connection) {
        return $channel = new AMQPChannel($connection);
    }

}
