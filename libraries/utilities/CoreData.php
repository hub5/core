<?php
namespace MQPublisher\Libraries\Utilities;

use MQPublisher\Config;
use MQPublisher\Libraries\Utilities\MQLogger;
use MQPublisher\Libraries\Utilities\ExecutionTime;
use MQPublisher\Libraries\Utilities\RedisConnection;
use MQPublisher\Libraries\Utilities\Utilities;

class CoreData{

	
    /**
     * Fetch service data
     * @param  int $serviceID serviceID
     * @return json  serviceData
     */
    function getServiceData($serviceID){
        $redis = new RedisConnection();
        $serviceKey = Config::COUNTRY_CODE.'.CORE.SERVICE.'.$serviceID;
        $serviceData = json_decode($redis->getKey($serviceKey));

        return $serviceData;
    }

    /**
     * Fetch service setting data
     * @param  int $serviceID serviceID
     * @return json serviceData
     */
    function getServiceSettingData($serviceID,$payerclientID){
        $redis = new RedisConnection();
        $serviceSettingKey = Config::COUNTRY_CODE.'.CORE.SERVICESETTING.'.$serviceID.'.'.$payerclientID;
        $serviceSettingData = json_decode($redis->getKey($serviceSettingKey));

        return $serviceSettingData;
    }

    /**
     * Fetch client id for api user posting payments
     * @param  string $username username
     * @return json clientDetails
     */
    function getClientDetails($username){
        $redis = new RedisConnection();
        $usernameKey = Config::COUNTRY_CODE.'.CORE.AUTH.'.$username;
        $clientDetails = json_decode($redis->getKey($usernameKey));

        return $clientDetails;
    }
    
}
