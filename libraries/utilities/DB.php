<?php
namespace MQPublisher\Libraries\Utilities;

use MQPublisher\Config;
use MQPublisher\Libraries\Utilities\MQLogger;
use MQPublisher\Libraries\Utilities\ExecutionTime;
use MQPublisher\Libraries\Utilities\RedisConnection;
use MQPublisher\Libraries\Utilities\Utilities;

class DB{

	
    /**
     * Create a database connection
     * @return object $dbh connection object
     */
    function getDBConnection(){
    	$host = Config::DB_HOST;
    	$database = Config::DATABASE;
    	$username = Config::DB_USERNAME;
    	$password = Config::DB_PASSWORD;
        $dbh = new \PDO("mysql:host=$host;dbname=$database", $username, $password);

        return $dbh;
    }
    
}
