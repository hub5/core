<?php
namespace MQPublisher\Libraries\Utilities;

class Utilities{

    /**
	 * Formats response as JSON
	 * @param  responseCode $code  Int
	 * @param  Description $descp Description of response
	 * @param  Data $data  Array of data in response
	 * @return String Object        Json object
	 */
	public static function response($code,$descp,$data){
		$response = array();
        $response['statusDescription'] = $descp;
        if(isset($data))
        	$response['data'] = $data;

        $response['code'] = $code;

        echo json_encode($response);
	}

	/**
	 * Formats array to csv
	 * @param  responseCode $code  Int
	 * @param  Description $descp Description of response
	 * @param  Data $data  Array of data in response
	 * @return String Object        Json object
	 */
	public static function arraytocsv($array){
		$csv = '';
		foreach ($array as $key => $value) {
			$csv .= $value.',';
		}

		//strip off the last comma
		$csv = rtrim($csv, ',');
        return $csv;
	}

	/**
	 * Date time with miliseconds
	 * @return String datetime datetime
	 */
	public static function dateTime(){
		$date = \DateTime::createFromFormat('U.u', microtime(TRUE));
        $date->setTimeZone(new \DateTimeZone('Africa/Nairobi'));

        return $date->format('Y-m-d H:i:s.u');
	}

	/**
	 * http Post
	 * @param  string $url endpoint to post to
	 * @param  fields $fields fields to post to endpoint
	 * @return mixed result depending on endpoint type i.e xml/json/text
	 */
	public static function post($url, $fields){
	    $fields_string = null;
	    $ch = curl_init();
	    //set the url, number of POST vars, POST data
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
	    curl_setopt($ch, CURLOPT_POST, count($fields));
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	    //execute post
	    $result = curl_exec($ch);
	    //close connection
	    curl_close($ch);

    	return $result;
	}

}